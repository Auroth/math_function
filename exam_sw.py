""" Importing libraries and external files """
import unittest
from math_function import hypot


class TDD(unittest.TestCase):
    """ Test Driven Development for math function """
    def test_correct_result(self):
        """ correct result """
        self.assertEqual(hypot(0, 2), 2.0)

    def test_missing_result(self):
        """ missing result """
        with self.assertRaises(TypeError):
            hypot(0, None)
        with self.assertRaises(TypeError):
            hypot("!@#$%^", 1234)

    def test_wrong_result(self):
        """ wrong result """
        self.assertNotEqual(hypot(-3, 3), 6)


if __name__ == "__main__":
    unittest.main()
