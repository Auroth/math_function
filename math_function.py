""" Importing libraries """
import math


def hypot(x, y):
    """ math function """
    return math.sqrt(x*x + y*y)
